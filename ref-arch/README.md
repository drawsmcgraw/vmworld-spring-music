![](gitlab-pipeline.jpg "Reference Architecture")

1. Developer commits code to GitLab repository triggering the pipeline.

2. A commit triggers a CD pipeline.

3. TBS combines an OS stack, buildpack, and code and outputs an OCI image.

4. The image is pushed into any public or private registry, for example, Harbor.

5. Registry is scanned for malware.

6. QA tests are run.

7. Image is pushed to target TKG cluster(s).
